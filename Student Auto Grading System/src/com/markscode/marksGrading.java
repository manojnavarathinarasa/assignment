package com.markscode;

import java.util.Scanner;

public class marksGrading {
	public static void main(String args[]) {

		Scanner scanner = new Scanner(System.in);

		double subject[] = { 0, 0, 0 };

		System.out.print("Enter First Subject Marks : ");
		subject[0] = scanner.nextDouble();
		System.out.print("Enter Second Subject Marks : ");
		subject[1] = scanner.nextDouble();
		System.out.print("Enter Thred Subject Marks : ");
		subject[2] = scanner.nextDouble();

		for (int i = 0; i < subject.length; i++) {
			// System.out.println("First Subject");

			if (subject[i] <= 100 && subject[i] >= 75) {

				System.out.println("Subject " + ((i) + 1) + " = " + subject[i] + "\t" + "A");

			} else if (subject[i] <= 74 && subject[i] >= 65) {

				System.out.println("Subject : " + ((i) + 1) + " = " + subject[i] + "\t" + "B");

			} else if (subject[i] <= 64 && subject[i] >= 55) {

				System.out.println("subject : " + ((i) + 1) + " = " + subject[i] + "\t" + "C");

			} else if (subject[i] <= 54 && subject[i] >= 35) {

				System.out.println("Subject : " + ((i) + 1) + " = " + subject[i] + "\t" + "S");

			} else if (subject[i] <= 34 && subject[i] >= 0) {

				System.out.println("Subject : " + ((i) + 1) + " = " + subject[i] + "\t" + "F");

			} else {

				System.err.println("please Enter Valied Marks");
			}
		}

		double total = (subject[0] + subject[1] + subject[2]);
		double avarage = total / 3;

		System.out.println("\nTotal: " + (subject[0] + " + " + subject[1] + " + " + subject[2]) + " = " + total);
		System.out.println("\nAvarage: " + avarage);
	}
}
