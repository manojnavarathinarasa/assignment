package com.login;

import java.util.Scanner;
import com.users.*;

public class Login {
	public void loginMethod() {

		Scanner scanner = new Scanner(System.in);

		Login loginCall = new Login();

		String userName = " ";
		String password = " ";

		// Admin
		String admin = "Admin";
		String passwordA = "Admin";

		// Teacher
		String teacherOneU = "Teacher01";
		String passwordT1 = "Teacher01";
		String teacherTwoU = "Teacher02";
		String passwordT2 = "Teacher02";
		String teacherThreeU = "Teacher03";
		String passwordT3 = "Teacher03";

		// Student
		String studentOneU = "Student01";
		String passwordS1 = "Student01";
		String studentTwoU = "Student02";
		String passwordS2 = "Student02";
		String studentThreeU = "Student03";
		String passwordS3 = "Student03";

		System.out.println("***Login Type***");
		System.out.println("01. Admin");
		System.out.println("02. Teachers");
		System.out.println("03. Student");
		System.out.println("04. Exit");

		System.out.print("Please Select This Option : ");

		int c = scanner.nextInt();
		if(c == 4 ) {
			System.out.println("Thanks Good Bye");
			System.exit(0);
		}
		if (c == 1 || c == 2 || c == 3) {

			System.out.print("Please Enter Your User Name : ");
			userName = scanner.next();

			System.out.print("Please Enter Your Password : ");
			password = scanner.next();

			switch (c) {
			case 1:
				if (userName.equals(admin) && password.equals(passwordA)) {		//Admin login validation part
					Admin adminCall = new Admin();								//Call Admin class
					adminCall.adminMin();										//Call admin class method

				} else {
					System.err.println(" Please Check Your User Name & Password ");

					loginCall.loginMethod();									//Recall login method
				}
				break;
			case 2:
				if (userName.equals(teacherOneU) && password.equals(passwordT1)) {	//Teacher login validation part
					Teachers teachersCall = new Teachers();							//Call Teacher class
					teachersCall.teachersMin();										//Call Teacher class method

				} else {
					System.err.println(" Please Check Your User Name & Password ");

					loginCall.loginMethod();										//Recall login method
				}
				break;
			case 3:
				if (userName.equals(studentOneU) && password.equals(passwordS1)) {	//Student login validation part
					Student studentCall = new Student();							//Call Student class
					studentCall.studentMin();										//Call Student class method

				} else {
					System.err.println(" Please Check Your User Name & Password ");

					loginCall.loginMethod();										//Recall login method
				}
				break;
			}
		} else {
			System.err.println("Please Select Correct Option");
			
			loginCall.loginMethod();
		}
	}

}
